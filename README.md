Planning this site

Keep it similar to the older version of my website (which lives in my memory).

I want to get a job using RoR, so I should probably build my site in RoR.  I can also build stand alone sites with React, Angular, Python as time goes by.

Navigation:
- Home
- About
- Projects
- Testimonials
- Resume Download
- Links
- Blog?

Animations
- Greensock

Tooling
- Haml, Handlebars
- React

Styles
- Base Bootstrap (or Semantic UI)

Images
- My Bio Pic

Content
- My Bio
- My Work
- Project Work
- My Skills

I am thinking RoR cause it'd be a good way for me to use my database skills, plus it's newer than PHP.  I could use Node.js too but I want to show everyone I have SQL skill, in addition to NoSQL.  So I'd like to do both, as the money is in being able to do stuff others can't.  On the front end, I can prob tool in React or Angular.

I don't think I really want to mention JavaScriptLA on this site, other than a small link.  I want to keep that separate from my work life.  The purpose of this resume site is to show off what all I can do as either a worker or a freelancer.  Later I can use this site as a model for making an agency version site.
